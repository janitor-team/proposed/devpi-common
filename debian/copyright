Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/devpi/devpi
Upstream-Name: devpi-common

Files: *
Copyright: 2013-2018 Holger Krekel, Florian Schulze
License: MIT

Files: devpi_common/vendor/_pip.py
Comment: Originally vendored from python3-pip
Copyright: 2008-2013 The pip developers:
    Alex Grönholm
    Alex Morega
    Alexandre Conrad
    Andrey Bulgakov
    Antti Kaihola
    Armin Ronacher
    Aziz Köksal
    Ben Rosser
    Brian Rosner
    Carl Meyer
    Chris McDonough
    Christian Oudard
    Clay McClure
    Cody Soyland
    Daniel Holth
    Dave Abrahams
    David (d1b)
    Dmitry Gladkov
    Donald Stufft
    Francesco
    Geoffrey Lehée
    Georgi Valkov
    Hugo Lopes Tavares
    Ian Bicking
    Igor Sobreira
    Ionel Maries Cristian
    Jakub Vysoky
    James Cleveland
    Jannis Leidel
    Jay Graves
    John-Scott Atlakson
    Jon Parise
    Jonas Nockert
    Josh Bronson
    Kamal Bin Mustafa
    Kelsey Hightower
    Kenneth Belitzky
    Kumar McMillan
    Luke Macken
    Masklinn
    Marc Abramowitz
    Marcus Smith
    Markus Hametner
    Matt Maker
    Maxime R.
    Miguel Araujo
    Nick Stenning
    Nowell Strite
    Oliver Tonnhofer
    Olivier Girardot
    Patrick Jenkins
    Paul Moore
    Paul Nasrat
    Paul Oswald
    Paul van der Linden
    Peter Waller
    Phil Whelan
    Piet Delport
    Przemek Wrzos
    Qiangning Hong
    Rafael Caricio
    Rene Dudfield
    Roey Berman
    Ronny Pfannschmidt
    Rory McCann
    Simon Cross
    Stavros Korokithakis
    Thomas Fenzl
    Thomas Johansson
    Vinay Sajip
    Vitaly Babiy
    W Trevor King
    Wil Tan
    Hsiaoming Yang
License: MIT

Files: devpi_common/vendor/_verlib.py
Comment: vendored from https://bitbucket.org/tarek/distutilsversion
Copyright: 2012 Tarek Ziadé and others
License: PSF-2

Files: debian/*
Copyright: 2018 Nicolas Dandrimont <olasd@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: PSF-2
 PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2
 --------------------------------------------
 .
 1. This LICENSE AGREEMENT is between the Python Software Foundation
 ("PSF"), and the Individual or Organization ("Licensee") accessing and
 otherwise using this software ("Python") in source or binary form and
 its associated documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, PSF
 hereby grants Licensee a nonexclusive, royalty-free, world-wide
 license to reproduce, analyze, test, perform and/or display publicly,
 prepare derivative works, distribute, and otherwise use Python
 alone or in any derivative version, provided, however, that PSF's
 License Agreement and PSF's notice of copyright, i.e., "Copyright (c)
 2001, 2002, 2003, 2004 Python Software Foundation; All Rights Reserved"
 are retained in Python alone or in any derivative version prepared
 by Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on
 or incorporates Python or any part thereof, and wants to make
 the derivative work available to others as provided herein, then
 Licensee hereby agrees to include in any such work a brief summary of
 the changes made to Python.
 .
 4. PSF is making Python available to Licensee on an "AS IS"
 basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND
 DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON WILL NOT
 INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
 A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON,
 OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material
 breach of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between PSF and
 Licensee.  This License Agreement does not grant permission to use PSF
 trademarks or trade name in a trademark sense to endorse or promote
 products or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using Python, Licensee
 agrees to be bound by the terms and conditions of this License
 Agreement.
